
public class AcumBooleanosParcial {
	
	
	
	static boolean mayor10(int [] lista) {
		boolean ret = true;
		for (int i= 0; i < lista.length ; i++) {
			ret = ret && lista[i]>10;
		}
		return ret;
	}
	
	
	static boolean pertenecenTodos(int[] elems,int [] arreglo) {
		boolean ret = true;
		for (int i = 0; i< elems.length ; i++) {
			ret = ret && seEncuentra(elems[i],arreglo);
		}
		return ret;
	}
	
	static boolean seEncuentra(int num,int[]arreglo) {
		boolean ret1= false;
		for (int i = 0 ; i <arreglo.length ; i++) {
			ret1= ret1 || arreglo[i]==num;
		}
		return ret1;
	}
	
	static boolean tieneNegativos(int[][]mat) {
		boolean ret = true;
		for (int fila = 0 ; fila < mat.length ; fila++) {
			ret = ret && negativoEnFila(mat,fila);
		}
		return ret;
	}
	
	static boolean negativoEnFila(int [][] mat , int fila) {
		boolean ret = false;
		for (int col = 0 ; col < mat[0].length; col++){
			ret = ret || mat[fila][col] < 0;
		}
		return ret;
	}
	
	static boolean estrictoAscendente (int [][] mat) {
		boolean ret = true;
		for (int fila = 0 ; fila < mat.length ; fila++) {
			ret = ret && ascendente(mat,fila);
		}
		return ret;
		
	}
	
	static boolean ascendente(int[][] mat,int fila) {
		boolean ret = true;
		int anterior = 0;
		for (int col= 0 ; col<mat[0].length ; col++) {
			ret = ret && mat[fila][col] > anterior;
			anterior = mat[fila][col];
		}
		return ret;
	}
	
	static boolean verificacionTotal (int[][] mat) {
		return estrictoAscendente(mat) && colParEImpar(mat);
		
	}
	
	static boolean colParEImpar (int [][] mat) {
		boolean ret = true;
		for (int col = 0 ; col < mat[0].length ; col++) {
			ret = ret && tienePar(mat,col) && tieneImpar (mat,col);
		}
		return ret;
		}
	
	static boolean tienePar (int[][] mat, int col) {
		boolean ret = false;
		for (int fila = 0 ; fila < mat.length ; fila++) {
			ret = ret || mat[fila][col] % 2 == 0;
		}
		return ret;
	}
	
	static boolean tieneImpar (int[][] mat, int col) {
		boolean ret = false;
		for (int fila = 0 ; fila < mat.length ; fila++) {
			ret = ret || mat[fila][col] % 2 != 0;
		}
		return ret;
	}
	
	//////// Ejercicio Parcial /////////
	
	static boolean arregloEnMatriz(int [][] mat , int[] arreglo) {
		boolean ret = true;
		for (int filaYArreglo = 0 ; filaYArreglo < mat.length;filaYArreglo++) {
			ret = ret && seEncuentraEnFila(mat,arreglo[filaYArreglo],filaYArreglo);
		}
		return ret;
	}
	
	static boolean seEncuentraEnFila (int[][] mat,int num , int fila) {
		boolean ret = false;
		for (int col = 0 ; col<mat[0].length ; col++) {
			ret = ret || mat[fila][col] == num;
		}
		return ret;
	}

}
