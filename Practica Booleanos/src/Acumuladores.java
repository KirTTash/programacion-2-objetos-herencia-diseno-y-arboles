import org.junit.Test;

public class Acumuladores {
		
	/// EJERCICIO 1
	@Test
	boolean columnaMultiploDeNumero(int [][]matriz,int col, int numero) {
		boolean ret = true;
		for (int fila= 0 ; fila<matriz.length; fila++) {
				ret = ret && matriz[fila][col] % numero == 0;
			}
		return ret;
		}
		
	
	boolean todosMultiplosColumna(int [][] matriz , int numero) {
		boolean ret = false;
		 if( numero<=0 || matriz.length==0 )
	        	return ret;
		for (int col = 0; col < matriz[0].length ; col++) {
				ret = ret || columnaMultiploDeNumero(matriz,col,numero); // << Esto ultimo && ... necesita un metodo aparte
			}
		return ret;
		}
	
	/// EJERCICIO 2
	
	@Test
	public boolean verificarConMatriz2(int [] mat1,int []mat2) { // Funcion Auxiliar 
		boolean ret = false;
		for (int iMat1 = 0 ; iMat1 < mat1.length ; iMat1++){
			for (int jMat2 = 0 ; jMat2 < mat2.length ; jMat2++) {
				ret = ret || mat1[iMat1] == mat2[jMat2];
			}
		}
		return ret;
	}
	
	public boolean hayInterseccionPorFila(int[][] mat1, int[][]mat2) { // Funcion Principal
		boolean ret = true;
		if(mat1.length!=mat2.length || mat1.length==0 || mat2.length==0)
			return false;
		for (int fila = 0; fila < mat1.length ; fila++) {
			for (int col = 0 ; col < mat1[fila].length ; col++) {
				ret = ret && verificarConMatriz2(mat1[fila],mat2[fila]);
			}
			
		}
		return ret;
	}
	
	//// EJERCICIO 3
	
	@Test
	public boolean algunaColumnaSumaMasQueLaFila(int[][] mat, int numFila) {// Funcion Principal
		boolean ret = false;
		if(numFila>=mat.length || numFila<0|| mat.length==0)
			return false;
		int SumarFila = 0;
		for (int col = 0 ; col < mat[0].length ; col++ ) {
			SumarFila += mat[numFila][col];
				}
		for (int col = 0 ; col < mat[0].length ; col ++) {
			ret = ret || columnaMayorQFila(mat,col,SumarFila);
		}
		return ret;
	}
	
	static boolean columnaMayorQFila(int[][] mat, int col, int sumaFila) {
		int sumaColumna=0;
		for (int indiceFila=0; indiceFila<mat.length;indiceFila++) {
			sumaColumna+=mat[indiceFila][col];
		}
		return sumaColumna>sumaFila;
	}
	
	//// EJERCICIO 4
	
	@Test
	public boolean hayInterseccionPorColumna(int[][] mat1, int[][]mat2) {
		boolean ret = true;
		if(mat1.length==0 || mat2.length==0 || mat1[0].length!=mat2[0].length )
			return false;
		for (int col = 0 ; col < mat1[0].length ; col++) {
			ret = ret && verificarColumna(mat1 ,mat2, col);
		}
		return ret;
	}
	
	public boolean verificarColumna(int [][] mat1, int [][] mat2, int numCol ) {
		boolean ret = false;
		for(int indiceFilaM1=0; indiceFilaM1<mat1.length;indiceFilaM1++) {
			for(int indiceFilaM2=0; indiceFilaM2<mat2.length;indiceFilaM2++) {
				ret=ret || mat1[indiceFilaM1][numCol] == mat2[indiceFilaM2][numCol];
				}
			}
		return ret;
		}
	
	
}


	
	
