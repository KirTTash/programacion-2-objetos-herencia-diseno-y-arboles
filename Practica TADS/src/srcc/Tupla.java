package srcc;

public class Tupla<T1,T2> {
	
	private T1 x_;
	private T2 y_;
	
	public Tupla (T1 x,T2 y) {
		x_ = x;
		y_ = y;
	}

	public T1 obtenerX() {
		return x_;
	}
	
	public T2 obtenerY() {
		return y_;
	}
	
	public void establecerX(T1 x) {
		x_ = x;
	}
	
	public void establecerY(T2 y) {
		y_ = y;
	}
}
