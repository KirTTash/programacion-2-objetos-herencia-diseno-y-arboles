package srcc;

public class TuplaDicc<C,S> {
	
	private C elem1_;
	private S elem2_;
	
	public TuplaDicc (C elem1,S elem2) {
		
		elem1_ = elem1;
		elem2_ = elem2;
	}
	
	public boolean equals(Object otro) {
		
		if (otro == null) {
			return false;
		}
		Tupla<C,S> tAux = (Tupla<C,S>) otro;
		return tAux.obtenerX().equals(elem1_);
	}

	public C obtenerC() {
		return elem1_;
	}
	
	public S obtenerS() {
		return elem2_;
	}
	
	public void establecerC(C elem1) {
		elem1_ = elem1;
	}
	
	public void establecerY(S elem2) {
		elem2_ = elem2;
	}
}
