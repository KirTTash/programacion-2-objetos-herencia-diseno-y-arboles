package srcc;

import java.util.HashMap;
import java.util.HashSet;

public class Biblioteca {
	HashMap<Libro,Integer> reservas;
	HashSet<Integer> dniPersonas;
	HashSet<Libro> librosDisponibles;
	Libro libroMasReservado;

	
	
	
	public Biblioteca() {
		this.reservas = new HashMap<Libro,Integer>();
		this.librosDisponibles = new HashSet<Libro>();
	}
	
	public void reservar(Libro libro,Integer dni) {
		if (reservas.containsKey(libro) && reservas.containsValue(dni)) {
			reservas.put(libro, dni);
			libro.setCantReservas(libro.getCantReservas()+1);
			if (this.libroMasReservado.getCantReservas() < libro.getCantReservas()) {
				this.libroMasReservado = libro;
			}else {
				librosDisponibles.remove(libro);
				dniPersonas.remove(dni);
			}	
		}
		if(!dniPersonas.contains(dni)) {
			throw new RuntimeException("No se encuentra el DNI o la persona ya reservo un libro");
		}
		else {
			throw new RuntimeException("No se encuentra el Libro o el libro ya fue reservado");
		}
	}
	
	public void devolver (Libro libro) {
		if(reservas.containsKey(libro)) {
			Integer dni = reservas.get(libro);
			librosDisponibles.add(libro);
			dniPersonas.add(dni);
		}else {
			throw new RuntimeException ("Este libro no fue reservado");
			}
		}
	
	public Libro libroMasReservado() {
		return this.libroMasReservado();
	}

	public void Agregar(Libro libro) {
		librosDisponibles.add(libro);
	}


}


