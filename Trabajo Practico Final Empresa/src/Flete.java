
public class Flete extends Transporte {
	
	protected int cantAcompaniantes;
	protected double costoPorAcompaniante;
	
	public Flete (String matricula,double cargaMax,double capacidad,double costoKM,int cantAcompaniantes,double costoPorAcompaniante) {
		super(matricula ,cargaMax ,capacidad ,costoKM ,false); // false porque el flete no tiene refrigeracion
		this.cantAcompaniantes = cantAcompaniantes;
		this.costoPorAcompaniante = costoPorAcompaniante;
	}

	@Override
	protected double obtenerCosto(Viaje viaje) {
		double suma=(super.costoPorKM*viaje.distanciaARecorrer)+(this.cantAcompaniantes*this.costoPorAcompaniante);
		return suma;
	}

	@Override
	protected boolean esValido(Integer distanciaARecorrer) {
		return true;
	}

}
