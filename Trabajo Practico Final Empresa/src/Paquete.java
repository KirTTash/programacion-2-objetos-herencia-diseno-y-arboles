
public class Paquete {
	// Atributos
	double peso;
	double volumen;
	String destino;
	boolean necesitaRefrigeracion;
	
	// Constructor
	
	public Paquete (String destino,double peso,double volumen,boolean necesitaRefrigeracion) {
		this.destino = destino;
		this.peso = peso;
		this.volumen = volumen;
		this.necesitaRefrigeracion = necesitaRefrigeracion;
	}
	
	// equals de Paquete
	
	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!this.getClass().equals(other.getClass())) {
			return false;
		}
		Paquete o = (Paquete) other;
		if (this.destino.equals(o.destino) && this.peso == o.peso && this.volumen == o.volumen){
			return true;
		}
		return false;
	}
}
