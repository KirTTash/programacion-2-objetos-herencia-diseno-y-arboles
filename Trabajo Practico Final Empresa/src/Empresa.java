
import java.util.*;

public class Empresa {
	protected String cuit;
	protected String nombre;
	protected int capacidadDeCadaDeposito; // Es el volumen de cada deposito, el peso es usado en sus camiones
	protected Map<String, Viaje> viajes;
	protected ArrayList<Deposito> depositos;
	protected Map<String, Transporte> transportes;
	protected Map<Transporte,Viaje> viajesAsignados;
	
	// Constructor Empresa
	
	public Empresa (String cuit,String nombre, int capacidadDeCadaDeposito) { // Constructor Empresa
		this.cuit = cuit;
		this.nombre = nombre;
		this.depositos=new ArrayList<Deposito>();
		this.viajes=new HashMap<String,Viaje>();
		this.transportes=new HashMap<String,Transporte>();
		this.viajesAsignados=new HashMap<Transporte,Viaje>();
		
		// Crear 2 depositos, uno frio y calor con el int de capacidad
		this.capacidadDeCadaDeposito = capacidadDeCadaDeposito;
		Deposito frio = new Deposito(capacidadDeCadaDeposito,true);
		Deposito normal = new Deposito(capacidadDeCadaDeposito,false);
		depositos.add(frio);  // Deposito 0 == Frio
		depositos.add(normal); // Deposito 1 == Normal
		
	}
	
	// ToString de empresa
	@Override
	public String toString() { // No se que mas se pide de un metodo to string
		String empresa = "La Empresa de Nombre: " +this.nombre + " y cuit: " + this.cuit + ". Posee " + transportes.size() + " transportes" ;
		return empresa;
	}
	
	
	
	// Incorpora un paquete a algun deposito de la empresa
	public boolean incorporarPaquete(String destino,double peso,double volumen, boolean necesitaRefrigeracion) {
		Paquete paq=new Paquete(destino,peso,volumen,necesitaRefrigeracion);	
		for (Deposito dep : depositos) {
			if(necesitaRefrigeracion == dep.tieneRefrigeracion && dep.espacioLibre>=volumen) 
				return dep.agregarPaq(paq);	
		}
		
		return false; // Si no se incorpora devuelve false, true en caso contrario
		
	}
	
	// Crea un viaje y lo agrega a la lista de Viajes
	public void agregarDestino(String destino,int km) {
		Viaje nuevoViaje = new Viaje(destino,km);
		if (viajes.containsKey(destino)) {//Condicional si el Viaje no se encuentra en el Set
			throw new RuntimeException("el viaje ya se encuentra dentro de la lista de viajes");
		}
		else
			viajes.put(destino, nuevoViaje);
	}
	
	// Agrego un Trailer a la empresa
	
	public void agregarTrailer(String matricula ,double cargaMax, double capacidad, boolean tieneRefrigeracion, double costoKM, double segCarga) {
		Trailer trailer = new Trailer(matricula,cargaMax,capacidad,tieneRefrigeracion,costoKM,segCarga);
		this.transportes.put(matricula,trailer);
	}
	
	// Agrego un MegaTrailer a la empresa
	
	public void agregarMegaTrailer(String matricula, double cargaMax, double capacidad, boolean tieneRefrigeracion, double costoKm, double segCarga, double costoFijo, double costoComida) {
		MegaTrailer megaT = new MegaTrailer(matricula,cargaMax, capacidad, tieneRefrigeracion, costoKm, segCarga, costoFijo, costoComida);
		this.transportes.put(matricula, megaT);
	}
		
	/// Agregar Flete a la empresa
	
	public void agregarFlete(String matricula, double cargaMax, double capacidad, double costoKM, int cantAcompaniantes, double costoPorAcompa�ante) {
		Flete flete = new Flete(matricula,cargaMax,capacidad,costoKM,cantAcompaniantes,costoPorAcompa�ante);
		this.transportes.put(matricula,flete);
		
	}
	
	// Asigno un destino a un transporte
	
	public void asignarDestino(String matricula, String destino) {
		
		Transporte trans= transportes.get(matricula);
		Viaje viaje=viajes.get(destino);
		
		if(!trans.esValido(viaje.distanciaARecorrer))
			throw new RuntimeException ("La distancia no es v�lida para este transporte");
		
		if(viajes.containsKey(destino) && transportes.containsKey(matricula) && !viajesAsignados.containsKey(transportes.get(matricula))) {
			viajesAsignados.put(transportes.get(matricula), viaje);
			transportes.get(matricula).asignarDestinoEnTrans(destino);
		}
		
		else
			if(!viajes.containsKey(destino))
				throw new RuntimeException("El destino no est� registrado");
			else
				if(!transportes.containsKey(matricula))
					throw new RuntimeException("El transporte no esta registrado");
	}
	
	public Deposito devolverDeposito(boolean tieneRefrigeracion) {
		for (Deposito deposito : depositos) {
			if (tieneRefrigeracion && deposito.tieneRefrigeracion) { // Si quiero que me de un deposito CON refrigeracion, y el deposito en el for each TIENE refrigeracion, devuelvo el deposito
				return deposito;
			}
			if (!tieneRefrigeracion && !(deposito.tieneRefrigeracion)) { // Si quiero que me de un deposito SIN refrigeracion, y el deposito en el for each NO TIENE refrigeracion, devuelvo el deposito
				return deposito;
			}
		}
		return null; // Como tieneRefrigeracion solo puede ser true o false, nunca va a devolver null ya que tengo los 2 posibles casos cubiertos
	}
	
	// Cargo un Transporte
	
	public double cargarTransporte(String matricula) {
		
		Deposito depo = null;
		if(!viajesAsignados.containsKey(transportes.get(matricula)))
			throw new RuntimeException("El transporte no tiene un destino asignado");
		else if(transportes.get(matricula).enViaje)
				throw new RuntimeException("El transporte se encuentra en viaje");
		else
			for(Deposito dep: this.depositos) {
				if(dep.tieneRefrigeracion == transportes.get(matricula).tieneRefrigeracion) {
					depo=dep;
				}
			}
		if(depo!=null) {
			
			ArrayList<Paquete> pallet = new ArrayList<Paquete>();
			pallet=depo.generarCarga(transportes.get(matricula).destino , transportes.get(matricula).capacidad, transportes.get(matricula).cargaMax);
			return transportes.get(matricula).cargarTrans(pallet);
		}
		return -1;
		
	}
	
	// Inicia el viaje del transporte identificado por su matricula
	
	public void iniciarViaje(String matricula) {
		if (transportes.get(matricula).carga.size() == 0) {
			throw new RuntimeException("El transporte no se encuentra cargado");
		}
		if (transportes.get(matricula).enViaje) {
			throw new RuntimeException("El transporte ya se encuentra en un viaje");
		}
		transportes.get(matricula).inicioViaje(); // 
	}
	
	// Finalizar viaje
	
	public void finalizarViaje(String matricula) {
		if (!transportes.get(matricula).enViaje) {
			throw new RuntimeException("El transporte no se encuentra en un viaje");
		}
		
		blanquearDestino(transportes.get(matricula));
		transportes.get(matricula).vaciarCarga();
		transportes.get(matricula).finalizoViaje(); // Finalizo el viaje del transporte
	}
	
	
	public void blanquearDestino(Transporte transporte) { // Elimina el key y value de viajesAsignados
		viajesAsignados.remove(transporte); // Como el transporte esta asociado a su viaje, tambien se elimina el viaje
	}                                       // Como viajesAsignados son los transportes que tienen viajes asignados. Entonces
	                                        // Eliminamos este value,key. Lo que dejaria al transporte sin viaje (el transporte se sigue encontrando en Set transportes)
	
	
	// Obtiene el costo de viaje del transporte  
	
	double obtenerCostoViaje(String matricula) {
		if(!transportes.containsKey(matricula))
				throw new RuntimeException("el transporte no esta registrado en la empresa");
			if(!transportes.get(matricula).enViaje)
				throw new RuntimeException ("el transporte no esta en viaje");
			else
				return transportes.get(matricula).obtenerCosto(viajesAsignados.get(transportes.get(matricula)));
	}
		
		public double devolverViajeAsignado(Transporte transporte) { // Devuelve la cantidad de KM que contiene el viaje de este transporte
			return viajesAsignados.get(transporte).distanciaARecorrer;
		
		}
	
	String obtenerTransporteIgual(String matricula) { // Como devuelve un string, el transporte requiere un to string
		Transporte transAComparar=transportes.get(matricula);
		
		for(Transporte trans : transportes.values()) {
			
			if(!trans.matricula.equals(transAComparar.matricula) && transAComparar.equals(trans))
				return trans.matricula;
		}		
		
		return null;
	}
} // llave de cierre de clase
