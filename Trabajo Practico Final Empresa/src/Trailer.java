
public class Trailer extends Transporte {
	
	protected double segCarga;
	protected int distanciaMaxima; 
	
	public Trailer(String matricula ,double cargaMax, double capacidad, boolean tieneRefrigeracion, double costoKM, double segCarga) {
		super(matricula,cargaMax,capacidad,costoKM,tieneRefrigeracion);
		this.segCarga = segCarga;
		this.distanciaMaxima = 500;
	}

	@Override
	protected double obtenerCosto(Viaje viaje) {
		double suma= (super.costoPorKM*viaje.distanciaARecorrer)+this.segCarga;
		return suma;
	}

	@Override
	protected boolean esValido(Integer distanciaARecorrer) {
		return distanciaARecorrer <= distanciaMaxima;
	}

}
