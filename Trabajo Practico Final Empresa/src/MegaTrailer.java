
public class MegaTrailer extends Transporte {
	
	protected double segCarga;
	protected double costoFijo; 
	protected double costoComida; 
	protected int distanciaMinima; 
	
	public MegaTrailer(String matricula, double cargaMax, double capacidad, boolean tieneRefrigeracion, double costoKm, double segCarga, double costoFijo, double costoComida) {
		super(matricula,cargaMax,capacidad,costoKm,tieneRefrigeracion);
		this.segCarga = segCarga;
		this.costoFijo = costoFijo;
		this.costoComida = costoComida;
		this.distanciaMinima = 500;
	}

	@Override
	protected double obtenerCosto(Viaje viaje) {
		double suma= (super.costoPorKM*viaje.distanciaARecorrer)+this.segCarga+this.costoComida+this.costoFijo;
		return suma;
	}

	@Override
	protected boolean esValido(Integer distanciaARecorrer) {
		return distanciaARecorrer >= distanciaMinima;
	}

}
