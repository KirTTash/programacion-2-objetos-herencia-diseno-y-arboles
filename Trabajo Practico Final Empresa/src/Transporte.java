
import java.util.*;

public abstract class Transporte { 
	// Variables
	protected String destino;
	protected String matricula; // id o Matricula 
	protected double cargaMax; // Peso
	protected double capacidad; // Volumen
	protected boolean tieneRefrigeracion;
	protected double costoPorKM;
	protected boolean enViaje;
	protected List<Paquete> carga;
	
	
	// Constructor 
	
	public Transporte (String matricula, double cargaMax, double capacidad, double costoPorKM, boolean tieneRefrigeracion) {
		this.matricula = matricula;
		this.cargaMax = cargaMax;
		this.capacidad = capacidad;
		this.costoPorKM = costoPorKM;
		this.tieneRefrigeracion = tieneRefrigeracion;
		this.enViaje = false;
		this.carga = new ArrayList<Paquete>();
		this.destino="";
		
	}
	
	// Equals
	
	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (! this.getClass().equals(other.getClass())) {
			return false;
		}
		Transporte o = (Transporte) other;
		if (this.destino.equals(o.destino) && this.carga.size() == o.carga.size() && tienenPaquetesIguales(this,o)) { 
			return true;
		}
		return false;
	}
	
	public boolean tienenPaquetesIguales(Transporte a, Transporte b) {
		boolean ret = true;
		for (Paquete paquete : a.carga) {
			if (b.carga.contains(paquete)) {
				ret = ret && true;
			}
		}
		return ret;
	}

	public double cargarTrans(ArrayList<Paquete> paquetes) {
		double volCargado=0;
		double pesoCargado=0;
		for(Paquete paq: paquetes) {
			if(volCargado+paq.volumen<=capacidad && pesoCargado+paq.peso<=cargaMax) {
				carga.add(paq);
				volCargado+=paq.volumen;
				pesoCargado+=paq.peso;
			}
			else
				break;
		}
		return volCargado;
	}
	
	public void inicioViaje() {
		this.enViaje=true;
	}

	public void finalizoViaje() {
		this.enViaje=false;
	}
	
	public void vaciarCarga() { // Vacia la carga del transporte 
		ArrayList<Paquete> cargaAVaciar=new ArrayList<Paquete>();
		for (Paquete paquete : this.carga) {
			cargaAVaciar.add(paquete);
		}
		for(Paquete p : cargaAVaciar) {
			this.carga.remove(p);
		}
	}

	protected abstract double obtenerCosto(Viaje viaje);

	public void asignarDestinoEnTrans(String dest) {
		this.destino=dest;
	}

	protected abstract boolean esValido(Integer distanciaARecorrer);
	
	
	

}