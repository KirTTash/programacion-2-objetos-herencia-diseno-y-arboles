import java.util.*;

public class Deposito {
	
	protected Map<String, ArrayList<Paquete>> paquetes;
	protected boolean tieneRefrigeracion;
	protected double capacidad; // Volumen
	protected double espacioLibre;
	
	public Deposito (Integer capacidad , boolean tieneRefrigeracion) {
		this.capacidad = capacidad;
		this.tieneRefrigeracion = tieneRefrigeracion;
		this.espacioLibre = capacidad;
		this.paquetes=new HashMap<String,ArrayList<Paquete>>();
	}
	
	public boolean agregarPaq(Paquete paq) {
		if(espacioLibre>=paq.volumen && paq.necesitaRefrigeracion==this.tieneRefrigeracion) {
			if(paquetes.containsKey(paq.destino))
				paquetes.get(paq.destino).add(paq);
			else {
				ArrayList<Paquete> listaPaq= new ArrayList<Paquete>();
				listaPaq.add(paq);
				paquetes.put(paq.destino, listaPaq);
			}
				
			this.espacioLibre-=paq.volumen;
			return true;
		}
			return false;
	}
	
	public ArrayList<Paquete> generarCarga(String destino, double volumen, double peso){
		ArrayList<Paquete> palletACargar=new ArrayList<Paquete>();
		
		for(Paquete paq: paquetes.get(destino)){
			if(volumen-paq.volumen>=0 && peso-paq.peso>=0) {
				palletACargar.add(paq);
				volumen-=paq.volumen;
				peso-=paq.peso;
			}
		}
		
		for(Paquete p: palletACargar) {
			paquetes.get(destino).remove(p);
		}
		
		return palletACargar;
	}
	
	
	

}
