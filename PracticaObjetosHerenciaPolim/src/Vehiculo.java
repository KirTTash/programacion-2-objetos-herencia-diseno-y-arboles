
public abstract class Vehiculo { /// Esta es mi superclase
	
	int cantidadRuedas;
	String nombre;
	
	public Vehiculo(String nombre,int cantidadRuedas) {
		this.nombre = nombre;
		this.cantidadRuedas = cantidadRuedas;
	}
	
	
	abstract String nombre();
	abstract int cantRuedas();
	
}
