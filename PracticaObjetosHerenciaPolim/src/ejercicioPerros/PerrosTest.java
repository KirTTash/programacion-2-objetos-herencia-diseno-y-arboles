package ejercicioPerros;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PerrosTest {
	
	@Test
	public void happypathtest() {
		Cocker cocker1 = new Cocker("SoyElCocker1", true, 4, true);
		Cocker cocker2 = new Cocker("SoyElCocker2", false, 4, false);
		Caniche caniche1 = new Caniche("SoyElCaniche1", true, 4, true);
		Caniche caniche2 = new Caniche("SoyElCaniche2", false, 4, false);
		assertTrue(cocker1.seLLevaBienCon(cocker2));
		assertTrue(caniche1.seLLevaBienCon(caniche2));
		assertFalse(cocker1.seLLevaBienCon(caniche2));
		System.out.println(cocker1.toString());
		System.out.println(cocker2.toString());
		System.out.println(caniche1.toString());
		System.out.println(caniche2.toString());
	}

}
