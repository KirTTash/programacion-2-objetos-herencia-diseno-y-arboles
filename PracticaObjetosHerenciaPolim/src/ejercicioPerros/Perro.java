package ejercicioPerros;

public abstract class Perro {
	String ladrido;
	Integer cantPatas;
	boolean muerde;
	boolean ladra;
	
	
	public Perro (String ladrido,boolean ladra, Integer cantPatas, boolean muerde) {
		this.ladrido = ladrido;
		this.ladra = ladra;
		this.cantPatas = cantPatas;
		this.muerde = muerde;
	}
	
	abstract String ladrar();
	abstract Integer cantPatas();
	abstract boolean seLLevaBienCon(Perro otroPerro);


}
