package ejercicioPerros;

public class Caniche extends Perro {
	
	public Caniche (String ladrido,boolean ladra, Integer cantPatas, boolean muerde) {
		super(ladrido,ladra,cantPatas,muerde);
	}
	
	@Override
	public String ladrar() {
		if (this.ladra) {
			return this.ladrido;
		}
		return "Este Caniche no ladra";
	}
	
	@Override
	public Integer cantPatas() {
		return this.cantPatas;
	}
	
	@Override
	public String toString() {
		if (this.ladra && this.muerde) {
			return "Su ladrido es:" + this.ladrido + " " +"y Muerde";
		}
		if (!this.ladra && !this.muerde) {
			return "Este Caniche no ladra ni muerde";
		}
		return "Su ladrido es:" + this.ladrido + " " +"y No Muerde";
	}
	
	@Override
	public boolean seLLevaBienCon(Perro otroPerro) {
		if (this.getClass().equals(otroPerro.getClass())) {
			return true;
		}
		return false;
	}

}

