package ejercicioPerros;

public class Cocker extends Perro {
	
	public Cocker (String ladrido,boolean ladra, Integer cantPatas, boolean muerde) {
		super(ladrido,ladra,cantPatas,muerde);
	}
	
	@Override
	public String ladrar() {
		if (this.ladra) {
			return this.ladrido;
		}
		return "Este Cocker no ladra";
	}
	
	@Override
	public Integer cantPatas() {
		return this.cantPatas;
	}
	
	@Override
	public String toString() {
		if (this.ladra && this.muerde) {
			return "Su ladrido es:" + this.ladrido + " " +"y Muerde";
		}
		if (!this.ladra && !this.muerde) {
			return "Este Cocker no ladra ni muerde";
		}
		return "Su ladrido es:" + this.ladrido + " " +"y No Muerde";
	}
	
	@Override
	public boolean seLLevaBienCon(Perro otroPerro) {
		if (this.getClass().equals(otroPerro.getClass())) {
			return true;
		}
		return false;
	}
		
}

