package ListaEnteros;

public class ListaRec {
	
	protected Nodo head; // Referencia al primer elemento
	private Integer size;
	
	
	public ListaRec() { 
		this.size = 0;
	}
	
	static class Nodo{
		Integer valor;
		Nodo tail;
		
		public Nodo (Integer valor) {
			this.valor=valor;
		}
		public Nodo (Nodo tail, Integer valor) {
			this.valor = valor;
			this.tail = tail;
		}
	}
	
	public void agregar(Integer elem) {
		head = agregar(head,elem);
	}
	
	private Nodo agregar(Nodo nodo,Integer elem) {
		if(nodo == null) {
			this.size++;
			return new Nodo(elem);
		}
		else {
			nodo.tail = agregar(nodo.tail,elem);{
				return nodo;
			}
		}
	}
	
	public Integer suma() {
		return suma(head);
	}
	
	private Integer suma(Nodo nodo) {
		if (nodo == null) {
			return 0;
		}
		else {
			return nodo.valor + suma(nodo.tail);
		}
	}

	public static String imprimirLista(ListaRec lista) {
		return imprimir(lista.head);
	}
	public static String imprimir (Nodo nodo) {
		if (nodo == null) {
			return "null";
		}else {
			return nodo.valor + imprimir (nodo.tail); 
		}		
	}
	
	public static Integer minimoLista(ListaRec a) {
//		if (a.size == 0) {
//			return 0; // Esto es una exepcion, esto no tiene que ocurrir
//		}
		if (a.size == 1){ // Este es mi caso base
			return a.head.valor;
		}
		return minimo(a.head.tail);
	}
	
	public static Integer minimo(Nodo a) {
		if (a.tail == null) {
			return a.valor;
		}
		return devolverMenorEntre(a.valor,minimo(a.tail));
	}
	
	static Integer devolverMenorEntre(Integer a, Integer b) {
		if(a < b) {
			return b;
		}
		return a;
	}
	
	public Integer minimo() {
        return minimoAux(this.head);
    }
    private Integer minimoAux(Nodo n) {
        if(n.tail==null)
            return n.valor;
        else
            return Math.min(n.valor, minimoAux(n.tail));
    }
    
    
    public Nodo reverse() {
    	return cambiarEnlace(this.head);
    }
    
    
    Nodo cambiarEnlace(Nodo head) {
    	if (head == null || head.tail == null) {
    		return head;
    	}
    	Nodo tail = cambiarEnlace(head.tail);
    	head.tail.tail = head;
    	head.tail = null;
    	
    	return tail;
    	
    }
	
	

	public Nodo getHead() {
		return head;
	}

	public void setHead(Nodo head) {
		this.head = head;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}
	
	


	
	}
