package Arboles;

import java.util.ArrayList;

public class Arboles<T> {
	Nodo<T> raiz;
	
	static class Nodo<T>{
		T valor;
		Nodo<T> izq;
		Nodo<T> der;
		
		
		public Nodo(T valor) {
			this.valor = valor;
		}
		
		public boolean equals(Nodo<T> nodo1 , Nodo<T> nodo2) { // Un nodo es igual a otro cuando cumple estas 3 condiciones
			boolean condicion1 = false;                           // 1-nodo1.valor == nodo2.valor
			boolean condicion2 = false;                           // 2-nodo1.der.valor == nodo2.der.valor
			boolean condicion3 = false;                           // 3-nodo1.izq.valor == nodo2.izq.valor
			if (nodo1.valor == nodo2.valor) {
				condicion1= true;;
			}
			if (nodo1.der.valor == nodo2.der.valor) {
				condicion2= true;;
			}
			if (nodo1.izq.valor == nodo2.izq.valor) {
				condicion3= true;;
			}
			return condicion1 && condicion2 && condicion3;
		}
		
	} /// llave de cierre de la clase statica nodo
	
	// Practica Arboles Binarios
	
	public Integer minimo() {
		return minimo(raiz);
	}
	private Integer minimo(Nodo<T> nodo) {
		if (esHoja(nodo)) {
			return (Integer) nodo.valor;
		}
		return (Integer) Math.min(minimo(nodo.der),minimo(nodo.izq)); 
	}
	
	
	
	
	
	/// Intento de resolucion para 1+ punto parcial
	
	// Funcion para detectar ciclos en arboles ordenados ( 1
	//                                                    /  \
	//                                                    2  3
	//                                                   /\  /\
	//                                                  4 5 6 7
	// Segun el patron de estos arboles, un ciclo se va a generar cuando el hijo de un nodo NO sea su 2ble o su 2ble+1
	
	public boolean tieneCiclosArbolOrdenado() { // <-- Metodo publico para el usuario
		return tieneCiclosArbolOrdenado(raiz);
	}
	
	private boolean tieneCiclosArbolOrdenado(Nodo<T> nodo){
		Integer ValorNodoActual = (Integer) nodo.valor;
		Integer ValorNodoIzquierdo = (Integer) nodo.izq.valor;
		Integer ValorNodoDerecho = (Integer) nodo.der.valor; 
		if (! (ValorNodoDerecho == ValorNodoActual * 2 || ValorNodoDerecho == ValorNodoActual * 2 +1)) {
			return true;
		}
		if (! (ValorNodoIzquierdo == ValorNodoActual * 2 || ValorNodoIzquierdo == ValorNodoActual * 2 +1)) {
			return true;
		}
		return tieneCiclosArbolOrdenado(nodo.der) || tieneCiclosArbolOrdenado(nodo.izq);
	}
	
	// Funcion para detectar ciclos en arboles Binarios ( 10
	// Cuyos valores Menores se alojan a la izquierda    /  \
	//  y valores mayores se alojan a la derecha         8  20
	//                                                   /\  /\
	//                                                  5 12 15 30
	
	// Segun este patron, debo detectar el enlace en si
	
	// " Creo un arraylist con todos los nodos del arbol. Cuando recorro un nodo, lo guardo en el array list."
	// " Un nodo tiene un enlace cuando nodo1.valor, nodo1.der.valor y nodo 1.izq.valor son == nodo2.valor, nodo2.der.valor y nodo 2.izq.valor
	// Para ello necesito comparar 2 nodos con un compareTo (de nodo)
	
	public boolean tieneCiclosArbolBinario() {
		ArrayList<T> listaNodos = new ArrayList<T>();
		return tieneCiclosArbolBinario(raiz,listaNodos);
	}
	
	private boolean tieneCiclosArbolBinario(Nodo<T> nodo, ArrayList<T> listaNodos) {  //
		if (esHoja(nodo)) {
			return false;
		}
		if (! seEncuentraElNodo(nodo,listaNodos)) {
				listaNodos.add(nodo); /// 
			}else {
				return true; // Hay un error en esto y es que siempre se crean nodos por cada comparacion
			}                // debo agregar un acumulador booleano, si es false, entonces adeo, si es true, el algoritmo retorna true
		return tieneCiclosArbolBinario(nodo.der, listaNodos) || tieneCiclosArbolBinario(nodo.izq, listaNodos);
	}
		
	
	/// Acumulador booleano
	private boolean seEncuentraElNodo (Nodo<T> nodo , Arraylist<T> listaNodos) {
		boolean ret = false;
		for (Nodo<T> nodoEn : listaNodos) {
			ret = Nodo.equals(nodo,nodoEn); // Si lo cambio a static,no puedo instanciarlos en tipo T
		}
		return ret;  // Dejo el algoritmo hasta aca porque tengo dudas de porque no me deja hacer esto
	}
	
	
	//1- Primero necesito crear un enlace entre los nodos ya existenes de un arbol
	// llamaremos a este nodo AgregarCiclo, que recibe 2 nodos ya existentes y los enlaza (para crear el ciclo, debemos enlazar 2 nodos que no se encuentren enlazados)
	
	public void crearCiclo(Integer ValorA, Integer ValorB) { // 
		crearCiclo(raiz, ValorA , ValorB);   // El enlace ocurre de la siguiente manera A ---- B (no tiene direccion) a.der = b b.iz= a
	}
	
	private void crearCiclo(Nodo<T> nodo , Integer ValorA ,Integer ValorB ) { // Para crear un ciclo, primero debemos localizar estos 2 nodos en el arbol, utilizaremos su valor como referencia (creo buscarNodo)
		BuscarNodo(nodo, ValorA).der = BuscarNodo(nodo,ValorB); // Nodo con ValorA.der = nodo con ValorB
		BuscarNodo(nodo,ValorB).izq = BuscarNodo(nodo, ValorA); // Nodo con ValorB.izq = nodo con valorA
	} // Especificamente esta funcion no crea ciclos, si no que crea enlaces. Solo si hacemos el enlace correcto crea ciclos
	
	private Nodo<T> BuscarNodo(Nodo<T> nodo , Integer ValorNodo) { //  Si estoy buscando un nodo, deberia retornar un nodo
		if (nodo == null) {
			return null;
		}
		
		if( mismoValor(nodo, ValorNodo)) { // Si el valor del nodo actual, es el mismo valor que el dado, retorno ese nodo
			return nodo;
		}
		if (mismoValor(nodo.izq, ValorNodo)) {
			return nodo.izq;
		}
		if (!mismoValor(nodo.izq, ValorNodo)) {
			return BuscarNodo(nodo.izq,ValorNodo);
		}
		
		if (!mismoValor(nodo.der, ValorNodo)) {
			return BuscarNodo(nodo.der,ValorNodo);
		}
		return nodo.der;
		
	}
	
	private boolean mismoValor(Nodo<T> nodo , Integer valor) {
		return nodo.valor == valor;
	}
	
	
	
	/// Metodo agregar nodo al arbol
	
	public void agregar(T elem) {
		if (raiz == null) {
			raiz = new Nodo<T>(elem);
		}else {
			agregar(raiz , elem);	
		}
		
	}
	
	private void agregar(Nodo<T> nodo , T elem) {
			if (nodo.izq == null) {
				Nodo<T> nuevo = new Nodo<T>(elem);
				nodo.izq = nuevo;
			}
			if (nodo.der == null) {
				Nodo<T> nuevo = new Nodo<T>(elem);
				nodo.der = nuevo;
			}else {
				agregar(nodo.izq, elem);
		
		}	
		
	}
	
	/// Detectar si el nodo es o no una hoja
	
	public boolean esHoja(Nodo<T> nodo) {  // Si su nodo.derecha es null y su nodo.izq es null entonces esto es una hoja, de lo contrario, no lo es
			return nodo.der == null && nodo.izq == null ; // Ya que la sentencia dentro del return es comparatoria, esto devuelve true o false sin necesidad de un if
	}
	
	// Sacar la Altura de un Arbol
	
	public Integer altura() {   // metodo publico para uso del usuario
		return altura(this.raiz);
	}
	
	private Integer altura(Nodo<T> nodo) { // La altura es la suma desde el nodo raiz hasta su hoja mas lejana
		if (esHoja(nodo)) { // Metodo privado para devolver el resultado
			return 1; // Si esto es una hoja, retorno 1 ya que cuento al nodo hoja
		}
		return 1 + Math.max(altura(nodo.der), altura(nodo.izq)); // Si no es hoja, es padre/nodo intermedio. Ya que es un nodo, lo sumo. y busco el maximo entre la altura del nodo derecho y el nodo izquierdo
		
	}
	
	// Verificar si un arbol esta o no balanceado
	
	public boolean balanceado() {
		return balanceado(this.raiz);
	}
	
	private boolean balanceado(Nodo<T> nodo) {
		if (esHoja(nodo)) {
			return true;
		}
		return Math.abs(altura(nodo.izq) - altura(nodo.der)) <= 1 && balanceado(nodo.izq) && balanceado(nodo.der);
	}
	
	// Contar la cantidad de nodos de un arbol
	public Integer cantNodos() {
		return cantNodos(this.raiz);
		}
	
	private Integer cantNodos(Nodo<T> nodo) {
		if (esHoja(nodo)) {
			return 1;
		}
		return 1 + cantNodos(nodo.der) + cantNodos(nodo.izq);
	}
	
	//  Busqueda que devuelve verdaro si algun nodo.dato  es elem
	
	public boolean buscar(Integer elem) {
		return buscar(raiz,elem);
	}
	
	private boolean buscar(Nodo<T> nodo,Integer valor) {
		if (esHoja(nodo) && ! (nodo.valor == valor)) {
			return false;
		}
		if (nodo.valor == valor) {
			return true;
		}
		return buscar(nodo.der,valor) || buscar(nodo.izq,valor);
	}

	
	
	
	

}// llave de cierre de la clase ArbolesBinarios
