package Arboles;

import java.util.ArrayList;
import java.util.List;

import Arboles.ArbolesBinarios.Nodo;

public class ArbolBinarioBusqueda<T extends Comparable<T>> extends Arboles<T> {
	
	@Override
	public void agregar(T elem){
		if(raiz== null){
			raiz = new Nodo<T>(elem);
		}else{
		this.agregar(raiz, elem);
		}
	}
	
	private void agregar(Nodo<T> nodo,T elem){
		if (elem.compareTo(nodo.valor) < 0){
			if (nodo.izq== null){
			nodo.izq= new Nodo<T>(elem);
			}else{
				agregar(nodo.izq,elem);
			}	
		}
		else{
			if(nodo.der == null){
				nodo.der = new Nodo<T>(elem);
			}else{
				agregar(nodo.der,elem);
			}
		}
		
		
	}
	public ArbolBinarioBusqueda<T> balancearArbol() {
		List<Nodo<T>> list = inOrder();
		ArbolBinarioBusqueda<T> actual = new ArbolBinarioBusqueda<T>();
		return balancear(actual,list);			
	}
	
	private ArbolBinarioBusqueda<T> balancear(ArbolBinarioBusqueda<T> actual, List<Nodo<T>> list){
		if (list.size() ==1) {
			actual.agregar(list.get(1).valor);
			return actual;
		}
		else {
			Integer medio = list.size() /2;
			actual.agregar(list.get(medio).valor);
			balancear(actual,(list.subList(0, medio)));
			balancear(actual,(list.subList(medio+1, list.size())));
			return actual;
			
		}
	}
	
	public List<Nodo<T>> inOrder() { // Devuelve una lista de nodos ordenados
		List<Nodo<T>> a = new ArrayList<Nodo<T>>();
		inOrder(raiz,a);
		return a;
	}
						
	
	private void inOrder(Nodo<T> n, List<Nodo<T>> lista) { // 
		  if(n!=null) {
		    inOrder(n.izq, lista);
		    lista.add(n);
		    inOrder(n.der, lista);
		  }
		}
	
	public int cantNodoPorNivel(int nivel) {
		return cantNodoPorNivel(raiz,0,nivel,0);
	}
	
	private int cantNodoPorNivel(Nodo<T> nodo,int cantNodo,int nivel,int nivelActual) {
		if (nodo == null) {
			cantNodo += 0;
			return cantNodo;
		}
		if (nivel == nivelActual) {
			cantNodo += 1;
			return cantNodo;
		}
		cantNodo += cantNodoPorNivel(nodo.der,cantNodo,nivel,nivelActual+1) 
					+ cantNodoPorNivel(nodo.izq,cantNodo,nivel,nivelActual+1);
			return cantNodo;
	}
		

	
}
