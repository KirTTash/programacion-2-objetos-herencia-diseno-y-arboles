package Arboles;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ArbolesBinariosTest {

	@Test
	public <T> void happyPathTest(){
		ArbolBinarioBusqueda<Integer> arbol = new ArbolBinarioBusqueda<Integer>();
		arbol.agregar(10);
		arbol.agregar(5);
		arbol.agregar(15);
		arbol.agregar(20);
		assertTrue(arbol.cantNodoPorNivel(1) == 2);
	}

}
